// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Particles/ParticleSystemComponent.h"
#include "StateEffect.generated.h"

UCLASS(Blueprintable, BlueprintType)
class VANDAL_API UStateEffect : public UObject
{
	GENERATED_BODY()

public:
	virtual bool IsSupportedForNetworking() const override { return true; }
	virtual bool InitObject(AActor* Actor, FName NameBoneHit);
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsStackable = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	UParticleSystem* ParticleEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	bool bIsAutoDestroyParticleEffect = false;

	UPROPERTY()
	AActor* MyActor = nullptr;
	UPROPERTY(Replicated)
	FName NameBone;
	UFUNCTION(NetMulticast, Reliable)
	void FxSpawnByStateEffect_Multicast(UParticleSystem* Effect, FName NameBoneHit);
};

UCLASS()
class VANDAL_API UStateEffect_ExecuteOnce : public UStateEffect
{
	GENERATED_BODY()

public:
	virtual bool InitObject(AActor* Actor, FName NameBoneHit) override;
	virtual void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
	float Power = 20.0f;
};

UCLASS()
class VANDAL_API UStateEffect_ExecuteTimer : public UStateEffect
{
	GENERATED_BODY()

public:
	virtual bool InitObject(AActor* Actor, FName NameBoneHit) override;
	virtual void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
};
