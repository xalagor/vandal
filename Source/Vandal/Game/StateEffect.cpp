// Fill out your copyright notice in the Description page of Project Settings.


#include "StateEffect.h"
#include "Net/UnrealNetwork.h"
#include "Vandal/Game/HealthComponent.h"
#include "Vandal/Interface/Vandal_IGameActor.h"

bool UStateEffect::InitObject(AActor* Actor, const FName NameBoneHit)
{
	MyActor = Actor;
	NameBone = NameBoneHit;
	const IVandal_IGameActor* MyInterface = Cast<IVandal_IGameActor>(MyActor);
	if (MyInterface)
	{
		MyInterface->Execute_AddEffect(MyActor, this);
	}

	return true;
}

void UStateEffect::DestroyObject()
{
	const IVandal_IGameActor* MyInterface = Cast<IVandal_IGameActor>(MyActor);
	if (MyInterface)
	{
		MyInterface->Execute_RemoveEffect(MyActor, this);
	}

	MyActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UStateEffect_ExecuteOnce::InitObject(AActor* Actor, const FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();
	return true;
}

void UStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UStateEffect_ExecuteOnce::ExecuteOnce()
{
	if (MyActor)
	{
		UHealthComponent* MyHealthComp = Cast<UHealthComponent>(MyActor->GetComponentByClass(UHealthComponent::StaticClass()));
		if (MyHealthComp)
		{
			MyHealthComp->ChangeHealthValue_OnServer(Power);
		}
	}

	DestroyObject();
}

bool UStateEffect_ExecuteTimer::InitObject(AActor* Actor, const FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UStateEffect_ExecuteTimer::DestroyObject, Timer,
		                                       false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UStateEffect_ExecuteTimer::Execute, RateTime, true);
	}
	return true;
}

void UStateEffect_ExecuteTimer::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}
	Super::DestroyObject();
}

void UStateEffect_ExecuteTimer::Execute()
{
	if (MyActor)
	{
		UHealthComponent* MyHealthComp = Cast<UHealthComponent>(MyActor->GetComponentByClass(UHealthComponent::StaticClass()));
		if (MyHealthComp)
		{
			MyHealthComp->ChangeHealthValue_OnServer(Power);
		}
	}
}

void UStateEffect::FxSpawnByStateEffect_Multicast_Implementation(UParticleSystem* Effect, FName NameBoneHit)
{
}

void UStateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UStateEffect, NameBone);
}
