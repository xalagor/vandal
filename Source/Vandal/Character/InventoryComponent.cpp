// Fill out your copyright notice in the Description page of Project Settings.

#include "InventoryComponent.h"
#include "Net/UnrealNetwork.h"
#include "Vandal/VandalGameInstance.h"
#include "Vandal/Interface/Vandal_IGameActor.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}

// Called every frame
void UInventoryComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UInventoryComponent::SwitchWeaponToIndexByNextPreviousIndex(const int32 ChangeToIndex, const int32 OldIndex, const FAdditionalWeaponInfo OldInfo,
                                                                 const bool bIsForward)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponSlots.Num() - 1)
	{
		CorrectIndex = 0;
	}
	else if (ChangeToIndex < 0)
	{
		CorrectIndex = WeaponSlots.Num() - 1;
	}

	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;
	int32 NewCurrentIndex = 0;

	if (WeaponSlots.IsValidIndex(CorrectIndex))
	{
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
		{
			if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
			{
				// good weapon have ammo start change
				bIsSuccess = true;
			}
			else
			{
				const UVandalGameInstance* MyGI = Cast<UVandalGameInstance>(GetWorld()->GetGameInstance());
				if (MyGI)
				{
					// check ammoSlots for this weapon
					FWeaponInfo MyInfo;
					MyGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, MyInfo);

					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlots.Num() && !bIsFind)
					{
						if (AmmoSlots[j].WeaponType == MyInfo.WeaponType && AmmoSlots[j].Count > 0)
						{
							// good weapon have ammo start change
							bIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}
				}
			}
			if (bIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
			}
		}
	}
	if (!bIsSuccess)
	{
		int8 Iteration = 0;
		int8 SecondIteration = 0;
		int8 TmpIndex;
		while (Iteration < WeaponSlots.Num() && !bIsSuccess)
		{
			Iteration++;

			if (bIsForward)
			{
				// SecondIteration = 0;
				TmpIndex = ChangeToIndex + Iteration;
			}
			else
			{
				SecondIteration = WeaponSlots.Num() - 1;

				TmpIndex = ChangeToIndex - Iteration;
			}
			if (WeaponSlots.IsValidIndex(TmpIndex))
			{
				if (!WeaponSlots[TmpIndex].NameItem.IsNone())
				{
					if (WeaponSlots[TmpIndex].AdditionalInfo.Round > 0)
					{
						// WeaponGood
						bIsSuccess = true;
						NewIdWeapon = WeaponSlots[TmpIndex].NameItem;
						NewAdditionalInfo = WeaponSlots[TmpIndex].AdditionalInfo;
						NewCurrentIndex = TmpIndex;
					}
					else
					{
						FWeaponInfo MyInfo;
						const UVandalGameInstance* MyGI = Cast<UVandalGameInstance>(GetWorld()->GetGameInstance());

						MyGI->GetWeaponInfoByName(WeaponSlots[TmpIndex].NameItem, MyInfo);

						bool bIsFind = false;
						int8 j = 0;
						while (j < AmmoSlots.Num() && !bIsFind)
						{
							if (AmmoSlots[j].WeaponType == MyInfo.WeaponType && AmmoSlots[j].Count > 0)
							{
								// WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[TmpIndex].NameItem;
								NewAdditionalInfo = WeaponSlots[TmpIndex].AdditionalInfo;
								NewCurrentIndex = TmpIndex;
								bIsFind = true;
							}
							j++;
						}
					}
				}
			}
			else
			{
				// go to end of left of array weapon slots
				if (OldIndex != SecondIteration)
				{
					if (WeaponSlots.IsValidIndex(SecondIteration))
					{
						if (!WeaponSlots[SecondIteration].NameItem.IsNone())
						{
							if (WeaponSlots[SecondIteration].AdditionalInfo.Round > 0)
							{
								// WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
								NewAdditionalInfo = WeaponSlots[SecondIteration].AdditionalInfo;
								NewCurrentIndex = SecondIteration;
							}
							else
							{
								FWeaponInfo MyInfo;
								const UVandalGameInstance* MyGI = Cast<UVandalGameInstance>(GetWorld()->GetGameInstance());

								MyGI->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, MyInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == MyInfo.WeaponType && AmmoSlots[j].Count > 0)
									{
										// WeaponGood
										bIsSuccess = true;
										NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
										NewAdditionalInfo = WeaponSlots[SecondIteration].AdditionalInfo;
										NewCurrentIndex = SecondIteration;
										bIsFind = true;
									}
									j++;
								}
							}
						}
					}
				}
				else
				{
					// go to same weapon when start
					if (WeaponSlots.IsValidIndex(SecondIteration))
					{
						if (!WeaponSlots[SecondIteration].NameItem.IsNone())
						{
							if (WeaponSlots[SecondIteration].AdditionalInfo.Round > 0)
							{
								// WeaponGood, it same weapon do nothing
							}
							else
							{
								FWeaponInfo MyInfo;
								const UVandalGameInstance* MyGI = Cast<UVandalGameInstance>(GetWorld()->GetGameInstance());

								MyGI->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, MyInfo);

								int8 j = 0;
								while (j < AmmoSlots.Num())
								{
									if (AmmoSlots[j].WeaponType == MyInfo.WeaponType)
									{
										if (AmmoSlots[j].Count > 0)
										{
											// WeaponGood, it same weapon do nothing
										}
										else
										{
											// Not find weapon with ammo need init Pistol with infinity ammo
											UE_LOG(LogTemp, Error, TEXT("UInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
										}
									}
									j++;
								}
							}
						}
					}
				}
				if (bIsForward)
				{
					SecondIteration++;
				}
				else
				{
					SecondIteration--;
				}
			}
		}
	}
	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);
		// OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
		SwitchWeaponEvent_OnServer(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
	}

	return bIsSuccess;
}

bool UInventoryComponent::SwitchWeaponByIndex(const int32 IndexWeaponToChange, const int32 PreviousIndex, const FAdditionalWeaponInfo PreviousWeaponInfo)
{
	bool bIsSuccess = false;

	const FName ToSwitchIdWeapon = GetWeaponNameBySlotIndex(IndexWeaponToChange);
	const FAdditionalWeaponInfo ToSwitchAdditionalInfo = GetAdditionalInfoWeapon(IndexWeaponToChange);

	if (!ToSwitchIdWeapon.IsNone())
	{
		SetAdditionalInfoWeapon(PreviousIndex, PreviousWeaponInfo);
		SwitchWeaponEvent_OnServer(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);
		// OnSwitchWeapon.Broadcast(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);

		// check ammo slot for event to player
		EWeaponType ToSwitchWeaponType;
		if (GetWeaponTypeByNameWeapon(ToSwitchIdWeapon, ToSwitchWeaponType))
		{
			int8 AvailableAmmoForWeapon = -1;
			if (CheckAmmoForWeapon(ToSwitchWeaponType, AvailableAmmoForWeapon))
			{
			}
		}
		bIsSuccess = true;
	}
	return bIsSuccess;
}

FAdditionalWeaponInfo UInventoryComponent::GetAdditionalInfoWeapon(const int32 IndexWeapon)
{
	FAdditionalWeaponInfo Result;
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				Result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);

	return Result;
}

int32 UInventoryComponent::GetWeaponIndexSlotByName(const FName IdWeaponName)
{
	int32 Result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			Result = i;
		}
		i++;
	}
	return Result;
}

FName UInventoryComponent::GetWeaponNameBySlotIndex(const int32 IndexSlot)
{
	FName Result;

	if (WeaponSlots.IsValidIndex(IndexSlot))
	{
		Result = WeaponSlots[IndexSlot].NameItem;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UInventoryComponent::GetWeaponNameBySlotIndex - Not Correct index Weapon  - %d"), IndexSlot);
	}
	return Result;
}

bool UInventoryComponent::GetWeaponTypeByIndexSlot(const int32 IndexSlot, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	WeaponType = EWeaponType::AssaultRifle;
	const UVandalGameInstance* MyGI = Cast<UVandalGameInstance>(GetWorld()->GetGameInstance());
	if (MyGI)
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			FWeaponInfo OutInfo;
			MyGI->GetWeaponInfoByName(WeaponSlots[IndexSlot].NameItem, OutInfo);
			WeaponType = OutInfo.WeaponType;
			bIsFind = true;
		}
	}
	return bIsFind;
}

bool UInventoryComponent::GetWeaponTypeByNameWeapon(const FName IdWeaponName, EWeaponType& WeaponType) const
{
	bool bIsFind = false;
	WeaponType = EWeaponType::AssaultRifle;
	const UVandalGameInstance* MyGI = Cast<UVandalGameInstance>(GetWorld()->GetGameInstance());
	if (MyGI)
	{
		FWeaponInfo OutInfo;
		MyGI->GetWeaponInfoByName(IdWeaponName, OutInfo);
		WeaponType = OutInfo.WeaponType;
		bIsFind = true;
	}
	return bIsFind;
}

void UInventoryComponent::SetAdditionalInfoWeapon(const int32 IndexWeapon, const FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;

				// OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
				WeaponAdditionalInfoChangeEvent_Multicast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UInventoryComponent::SetAdditionalInfoWeapon - Not Found Weapon with index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);
}

void UInventoryComponent::AmmoSlotChangeValue(const EWeaponType TypeWeapon, const int32 CountChangeAmmo)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Count += CountChangeAmmo;
			if (AmmoSlots[i].Count > AmmoSlots[i].MaxCount)
			{
				AmmoSlots[i].Count = AmmoSlots[i].MaxCount;
			}
			AmmoChangeEvent_Multicast(AmmoSlots[i].WeaponType, AmmoSlots[i].Count);
			// OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Count);

			bIsFind = true;
		}
		i++;
	}
}

bool UInventoryComponent::CheckAmmoForWeapon(const EWeaponType TypeWeapon, int8& AvailableAmmoForWeapon)
{
	AvailableAmmoForWeapon = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;
			AvailableAmmoForWeapon = AmmoSlots[i].Count;
			if (AmmoSlots[i].Count > 0)
			{
				return true;
			}
		}
		i++;
	}

	if (AvailableAmmoForWeapon <= 0)
	{
		WeaponAmmoEmptyEvent_Multicast(TypeWeapon);
		// OnWeaponAmmoEmpty.Broadcast(TypeWeapon);
	}
	else
	{
		WeaponAmmoAvailableEvent_Multicast(TypeWeapon);
		// OnWeaponAmmoAvailable.Broadcast(TypeWeapon);
	}

	return false;
}

bool UInventoryComponent::CheckCanTakeAmmo(const EWeaponType AmmoType)
{
	bool bResult = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bResult)
	{
		if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Count < AmmoSlots[i].MaxCount)
		{
			bResult = true;
		}
		i++;
	}
	return bResult;
}

bool UInventoryComponent::CheckCanTakeWeapon(int32& FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsFreeSlot)
	{
		if (WeaponSlots[i].NameItem.IsNone())
		{
			bIsFreeSlot = true;
			FreeSlot = i;
		}
		i++;
	}
	return bIsFreeSlot;
}

bool UInventoryComponent::SwitchWeaponToInventory(const FWeaponSlot NewWeapon, const int32 IndexSlot, const int32 CurrentIndexWeaponChar,
                                                  FDropItem& DropItemInfo)
{
	bool bResult = false;

	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
	{
		WeaponSlots[IndexSlot] = NewWeapon;

		SwitchWeaponToIndexByNextPreviousIndex(CurrentIndexWeaponChar, -1, NewWeapon.AdditionalInfo, true);

		// OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
		UpdateWeaponSlotsEvent_Multicast(IndexSlot, NewWeapon);
		bResult = true;
	}
	return bResult;
}

void UInventoryComponent::TryGetWeaponToInventory_OnServer_Implementation(AActor* PickUpActor, const FWeaponSlot NewWeapon)
{
	int32 IndexSlot = -1;
	if (CheckCanTakeWeapon(IndexSlot))
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			WeaponSlots[IndexSlot] = NewWeapon;

			// OnUpdateWeaponSlots.Broadcast(indexSlot, NewWeapon);
			UpdateWeaponSlotsEvent_Multicast(IndexSlot, NewWeapon);

			if (PickUpActor)
			{
				PickUpActor->Destroy();
			}
		}
	}
}

void UInventoryComponent::DropWeaponByIndex_OnServer_Implementation(const int32 ByIndex)
{
	FDropItem DropItemInfo;

	bool bIsCanDrop = false;
	int8 i = 0;
	int8 AvailableWeaponNum = 0;
	while (i < WeaponSlots.Num() && !bIsCanDrop)
	{
		if (!WeaponSlots[i].NameItem.IsNone())
		{
			AvailableWeaponNum++;
			if (AvailableWeaponNum > 1)
			{
				bIsCanDrop = true;
			}
		}
		i++;
	}

	if (bIsCanDrop && WeaponSlots.IsValidIndex(ByIndex) && GetDropItemInfoFromInventory(ByIndex, DropItemInfo))
	{
		const FWeaponSlot EmptyWeaponSlot;
		int8 j = 0;
		while (j < WeaponSlots.Num())
		{
			if (!WeaponSlots[j].NameItem.IsNone())
			{
				SwitchWeaponEvent_OnServer(WeaponSlots[j].NameItem, WeaponSlots[j].AdditionalInfo, j);
				// OnSwitchWeapon.Broadcast(WeaponSlots[j].NameItem, WeaponSlots[j].AdditionalInfo, j);
			}
			j++;
		}

		WeaponSlots[ByIndex] = EmptyWeaponSlot;
		if (GetOwner()->GetClass()->ImplementsInterface(UVandal_IGameActor::StaticClass()))
		{
			IVandal_IGameActor::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);
		}

		// OnUpdateWeaponSlots.Broadcast(ByIndex, EmptyWeaponSlot);
		UpdateWeaponSlotsEvent_Multicast(ByIndex, EmptyWeaponSlot);
	}
}

bool UInventoryComponent::GetDropItemInfoFromInventory(const int32 IndexSlot, FDropItem& DropItemInfo)
{
	bool bResult = false;

	const FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);

	const UVandalGameInstance* MyGI = Cast<UVandalGameInstance>(GetWorld()->GetGameInstance());
	if (MyGI)
	{
		bResult = MyGI->GetDropItemInfoByWeaponName(DropItemName, DropItemInfo);
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
		}
	}

	return bResult;
}

TArray<FWeaponSlot> UInventoryComponent::GetWeaponSlots()
{
	return WeaponSlots;
}

TArray<FAmmoSlot> UInventoryComponent::GetAmmoSlots()
{
	return AmmoSlots;
}

void UInventoryComponent::InitInventory_OnServer_Implementation(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo)
{
	WeaponSlots = NewWeaponSlotsInfo;
	AmmoSlots = NewAmmoSlotsInfo;

	MaxSlotsWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
		{
			// OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
			SwitchWeaponEvent_OnServer(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
		}
	}
}

void UInventoryComponent::AmmoChangeEvent_Multicast_Implementation(const EWeaponType TypeWeapon, const int32 Count)
{
	OnAmmoChange.Broadcast(TypeWeapon, Count);
}

void UInventoryComponent::SwitchWeaponEvent_OnServer_Implementation(const FName WeaponName, const FAdditionalWeaponInfo AdditionalInfo, const int32 IndexSlot)
{
	OnSwitchWeapon.Broadcast(WeaponName, AdditionalInfo, IndexSlot);
}

void UInventoryComponent::WeaponAdditionalInfoChangeEvent_Multicast_Implementation(const int32 IndexSlot, const FAdditionalWeaponInfo AdditionalInfo)
{
	OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, AdditionalInfo);
}

void UInventoryComponent::WeaponAmmoEmptyEvent_Multicast_Implementation(const EWeaponType TypeWeapon)
{
	OnWeaponAmmoEmpty.Broadcast(TypeWeapon);
}

void UInventoryComponent::WeaponAmmoAvailableEvent_Multicast_Implementation(const EWeaponType TypeWeapon)
{
	OnWeaponAmmoAvailable.Broadcast(TypeWeapon);
}

void UInventoryComponent::UpdateWeaponSlotsEvent_Multicast_Implementation(const int32 IndexSlotChange, const FWeaponSlot NewInfo)
{
	OnUpdateWeaponSlots.Broadcast(IndexSlotChange, NewInfo);
}

void UInventoryComponent::WeaponNotHaveRoundEvent_Multicast_Implementation(const int32 IndexSlotWeapon)
{
	OnWeaponNotHaveRound.Broadcast(IndexSlotWeapon);
}

void UInventoryComponent::WeaponHaveRoundEvent_Multicast_Implementation(const int32 IndexSlotWeapon)
{
	OnWeaponHaveRound.Broadcast(IndexSlotWeapon);
}

void UInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UInventoryComponent, WeaponSlots);
	DOREPLIFETIME(UInventoryComponent, AmmoSlots);
}
