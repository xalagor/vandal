// Fill out your copyright notice in the Description page of Project Settings.

#include "CharacterHealthComponent.h"

void UCharacterHealthComponent::ChangeHealthValue_OnServer(const float ChangeValue)
{
	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);

		if (Shield < 0.0f)
		{
			// FX
			// UE_LOG(LogTemp, Warning, TEXT("UCharacterHealthComponent::ChangeHealthValue_OnServer - Shield < 0"));
		}
	}
	else
	{
		Super::ChangeHealthValue_OnServer(ChangeValue);
	}
}

float UCharacterHealthComponent::GetCurrentShield() const
{
	return Shield;
}

void UCharacterHealthComponent::ChangeShieldValue(const float ChangeValue)
{
	Shield += ChangeValue;
	// OnShieldChange.Broadcast(Shield, ChangeValue);
	ShieldChangeEvent_Multicast(Shield, ChangeValue);

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime,
		                                       false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
}

void UCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UCharacterHealthComponent::RecoveryShield()
{
	float Tmp = Shield;
	Tmp = Tmp + ShieldRecoverValue;
	if (Tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = Tmp;
	}

	// OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
	ShieldChangeEvent_Multicast(Shield, ShieldRecoverValue);
}

float UCharacterHealthComponent::GetShieldValue() const
{
	return Shield;
}

void UCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(const float NewShield, const float Damage)
{
	OnShieldChange.Broadcast(NewShield, Damage);
}
