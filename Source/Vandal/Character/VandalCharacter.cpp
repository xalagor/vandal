// Copyright Epic Games, Inc. All Rights Reserved.

#include "VandalCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/DecalComponent.h"
#include "Engine/ActorChannel.h"
#include "Engine/World.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Materials/Material.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "UObject/ConstructorHelpers.h"

#include "Vandal/VandalGameInstance.h"
#include "Vandal/Game/Projectile.h"

AVandalCharacter::AVandalCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
	CharHealthComponent = CreateDefaultSubobject<UCharacterHealthComponent>(TEXT("HealthComponent"));

	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &AVandalCharacter::CharDead);
	}
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AVandalCharacter::InitWeapon);
	}
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	// NetWork
	bReplicates = true;
}

void AVandalCharacter::Tick(const float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		const APlayerController* MyPC = Cast<APlayerController>(GetController());
		if (MyPC && MyPC->IsLocalPlayerController())
		{
			FHitResult TraceHitResult;
			MyPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			const FVector CursorFv = TraceHitResult.ImpactNormal;
			const FRotator CursorR = CursorFv.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void AVandalCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		if (CursorMaterial && (GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority))
		{
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		}
	}
}

void AVandalCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AVandalCharacter::InputAxisX);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AVandalCharacter::InputAxisY);

	PlayerInputComponent->BindAction(TEXT("ChangeToWalk"), IE_Pressed, this, &AVandalCharacter::InputWalkPressed);
	PlayerInputComponent->BindAction(TEXT("ChangeToSprint"), IE_Pressed, this, &AVandalCharacter::InputSprintPressed);
	PlayerInputComponent->BindAction(TEXT("AimEvent"), IE_Pressed, this, &AVandalCharacter::InputAimPressed);
	PlayerInputComponent->BindAction(TEXT("ChangeToSprint"), IE_Released, this, &AVandalCharacter::InputSprintReleased);
	PlayerInputComponent->BindAction(TEXT("ChangeToWalk"), IE_Released, this, &AVandalCharacter::InputWalkReleased);
	PlayerInputComponent->BindAction(TEXT("AimEvent"), IE_Released, this, &AVandalCharacter::InputAimReleased);

	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &AVandalCharacter::InputAttackPressed);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &AVandalCharacter::InputAttackReleased);
	PlayerInputComponent->BindAction(TEXT("Reload"), IE_Released, this, &AVandalCharacter::TryReloadWeapon);

	PlayerInputComponent->BindAction(TEXT("SwitchNextWeapon"), IE_Pressed, this, &AVandalCharacter::TrySwitchNextWeapon);
	PlayerInputComponent->BindAction(TEXT("SwitchPreviousWeapon"), IE_Pressed, this, &AVandalCharacter::TrySwitchPreviousWeapon);

	PlayerInputComponent->BindAction(TEXT("Ability"), IE_Pressed, this, &AVandalCharacter::TryAbilityEnabled);

	PlayerInputComponent->BindAction(TEXT("DropCurrentWeapon"), IE_Pressed, this, &AVandalCharacter::DropCurrentWeapon);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	PlayerInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &AVandalCharacter::TKeyPressed<1>);
	PlayerInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &AVandalCharacter::TKeyPressed<2>);
	PlayerInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &AVandalCharacter::TKeyPressed<3>);
	PlayerInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &AVandalCharacter::TKeyPressed<4>);
	PlayerInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &AVandalCharacter::TKeyPressed<5>);
	PlayerInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &AVandalCharacter::TKeyPressed<6>);
	PlayerInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &AVandalCharacter::TKeyPressed<7>);
	PlayerInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &AVandalCharacter::TKeyPressed<8>);
	PlayerInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &AVandalCharacter::TKeyPressed<9>);
	PlayerInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &AVandalCharacter::TKeyPressed<0>);
}

void AVandalCharacter::InputAxisY(const float Value)
{
	AxisY = Value;
}

void AVandalCharacter::InputAxisX(const float Value)
{
	AxisX = Value;
}

// ReSharper disable once CppMemberFunctionMayBeConst
void AVandalCharacter::InputAttackPressed()
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive())
	{
		AttackCharEvent(true);
	}
}

// ReSharper disable once CppMemberFunctionMayBeConst
void AVandalCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void AVandalCharacter::InputWalkPressed()
{
	ChangeMovementState(EMovementState::Walk_State);
}

void AVandalCharacter::InputWalkReleased()
{
	ChangeMovementState(EMovementState::Run_State);
}

void AVandalCharacter::InputSprintPressed()
{
	ChangeMovementState(EMovementState::Sprint_State);
}

void AVandalCharacter::InputSprintReleased()
{
	ChangeMovementState(EMovementState::Run_State);
}

void AVandalCharacter::InputAimPressed()
{
	if (GetMovementState() == EMovementState::Walk_State)
	{
		ChangeMovementState(EMovementState::AimWalk_State);
	}
	else
	{
		ChangeMovementState(EMovementState::Aim_State);
	}
}

void AVandalCharacter::InputAimReleased()
{
	if (GetMovementState() == EMovementState::AimWalk_State)
	{
		ChangeMovementState(EMovementState::Walk_State);
	}
	else
	{
		ChangeMovementState(EMovementState::Run_State);
	}
}

void AVandalCharacter::MovementTick(float /*DeltaTime*/)
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive())
	{
		if (GetController() && GetController()->IsLocalPlayerController())
		{
			AddMovementInput(FVector(1.f, 0.f, 0.f), AxisX);
			AddMovementInput(FVector(0.f, 1.f, 0.f), AxisY);

			if (MovementState == EMovementState::Sprint_State)
			{
				const FVector MyRotationVector = FVector(AxisX, AxisY, 0.f);
				const FRotator MyRotator = MyRotationVector.ToOrientationRotator();
				SetActorRotation(FQuat(MyRotator));
				SetActorRotationByYaw_OnServer(MyRotator.Yaw);
			}
			else
			{
				const APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
				if (MyController)
				{
					FHitResult ResultHit;
					// myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
					//  bug was here Config\DefaultEngine.Ini
					MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

					const float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
					SetActorRotation(FQuat(FRotator(0.f, FindRotatorResultYaw, 0.f)));
					SetActorRotationByYaw_OnServer(FindRotatorResultYaw);
					if (CurrentWeapon)
					{
						FVector Displacement = FVector(0);
						bool bIsReduceDispersion = false;
						switch (MovementState)
						{
							case EMovementState::Aim_State:
								Displacement = FVector(0.f, 0.f, 160.f);
							// CurrentWeapon->ShouldReduceDispersion = true;
								bIsReduceDispersion = true;
								break;
							case EMovementState::AimWalk_State:
								Displacement = FVector(0.f, 0.f, 160.f);
							// CurrentWeapon->ShouldReduceDispersion = true;
								bIsReduceDispersion = true;
								break;
							case EMovementState::Walk_State:
								Displacement = FVector(0.f, 0.f, 120.f);
							// CurrentWeapon->ShouldReduceDispersion = false;
								break;
							case EMovementState::Run_State:
								Displacement = FVector(0.f, 0.f, 120.f);
							// CurrentWeapon->ShouldReduceDispersion = false;
								break;
							case EMovementState::Sprint_State:
								break;
							default:
								break;
						}

						// CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
						CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(ResultHit.Location + Displacement, bIsReduceDispersion);
						// aim cursor like 3d Widget?
					}
				}
			}
		}
	}
}

EMovementState AVandalCharacter::GetMovementState() const
{
	return MovementState;
}

TArray<UStateEffect*> AVandalCharacter::GetCurrentEffectsOnChar()
{
	return Effects;
}

int32 AVandalCharacter::GetCurrentWeaponIndex() const
{
	return CurrentIndexWeapon;
}

bool AVandalCharacter::GetIsAlive() const
{
	bool Result = false;
	if (CharHealthComponent)
	{
		Result = CharHealthComponent->GetIsAlive();
	}
	return Result;
}

void AVandalCharacter::AttackCharEvent(const bool bIsFiring) const
{
	AWeapon* MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("AVandalCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void AVandalCharacter::CharacterUpdate() const
{
	float ResSpeed;
	switch (MovementState)
	{
		case EMovementState::Aim_State:
			ResSpeed = MovementSpeedInfo.AimSpeedNormal;
			break;
		case EMovementState::AimWalk_State:
			ResSpeed = MovementSpeedInfo.AimSpeedWalk;
			break;
		case EMovementState::Walk_State:
			ResSpeed = MovementSpeedInfo.WalkSpeed;
			break;
		case EMovementState::Run_State:
			ResSpeed = MovementSpeedInfo.RunSpeed;
			break;
		case EMovementState::Sprint_State:
			ResSpeed = MovementSpeedInfo.SprintSpeed;
			break;
		default:
			ResSpeed = MovementSpeedInfo.RunSpeed;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void AVandalCharacter::ChangeMovementState(const EMovementState NewMovementState)
{
	const EMovementState NewState = NewMovementState;

	SetMovementState_OnServer(NewState);

	// CharacterUpdate();

	// Weapon state update
	AWeapon* MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->UpdateStateWeapon_OnServer(NewState);
	}
}

AWeapon* AVandalCharacter::GetCurrentWeapon() const
{
	return CurrentWeapon;
}

void AVandalCharacter::InitWeapon(const FName IdWeaponName, const FAdditionalWeaponInfo WeaponAdditionalInfo, const int32 NewCurrentIndexWeapon)
{
	// On server
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	const UVandalGameInstance* MyGI = Cast<UVandalGameInstance>(GetGameInstance());
	if (MyGI)
	{
		FWeaponInfo MyWeaponInfo;
		if (MyGI->GetWeaponInfoByName(IdWeaponName, MyWeaponInfo))
		{
			if (MyWeaponInfo.WeaponClass)
			{
				const FVector SpawnLocation = FVector(0);
				const FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeapon* MyWeapon = Cast<AWeapon>(GetWorld()->SpawnActor(MyWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (MyWeapon)
				{
					const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					MyWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = MyWeapon;
					MyWeapon->IdWeaponName = IdWeaponName;
					MyWeapon->WeaponSetting = MyWeaponInfo;

					MyWeapon->ReloadTime = MyWeaponInfo.ReloadTime;
					MyWeapon->UpdateStateWeapon_OnServer(MovementState);

					MyWeapon->WeaponInfo = WeaponAdditionalInfo;
					CurrentIndexWeapon = NewCurrentIndexWeapon;

					MyWeapon->OnWeaponReloadStart.AddDynamic(this, &AVandalCharacter::WeaponReloadStart);
					MyWeapon->OnWeaponReloadEnd.AddDynamic(this, &AVandalCharacter::WeaponReloadEnd);

					MyWeapon->OnWeaponFire.AddDynamic(this, &AVandalCharacter::WeaponFire);

					// after switch try reload weapon if needed
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
					{
						CurrentWeapon->InitReload();
					}

					if (InventoryComponent)
					{
						InventoryComponent->OnWeaponAmmoAvailable.Broadcast(MyWeapon->WeaponSetting.WeaponType);
					}
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("AVandalCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void AVandalCharacter::TryReloadWeapon()
{
	if (CharHealthComponent && CharHealthComponent->GetIsAlive() && CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		TryReloadWeapon_OnServer();
	}
}

void AVandalCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void AVandalCharacter::WeaponReloadEnd(const bool bIsSuccess, const int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void AVandalCharacter::TrySwitchWeaponToIndexByKeyInput_OnServer_Implementation(const int32 ToIndex)
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			const int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->WeaponInfo;
				if (CurrentWeapon->WeaponReloading)
				{
					CurrentWeapon->CancelReload();
				}
			}

			InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}
}

// ReSharper disable once CppMemberFunctionMayBeConst
void AVandalCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		FDropItem ItemInfo;
		InventoryComponent->DropWeaponByIndex_OnServer(CurrentIndexWeapon);
	}
}

void AVandalCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void AVandalCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// in BP
}

void AVandalCharacter::WeaponFire(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}
	WeaponFire_BP(Anim);
}

void AVandalCharacter::WeaponFire_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

UDecalComponent* AVandalCharacter::GetCursorToWorld() const
{
	return CurrentCursor;
}

// ReSharper disable once CppMemberFunctionMayBeConst
void AVandalCharacter::TrySwitchNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		// We have more then one weapon go switch
		const int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviousIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}
}

// ReSharper disable once CppMemberFunctionMayBeConst
void AVandalCharacter::TrySwitchPreviousWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		// We have more then one weapon go switch
		const int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (InventoryComponent)
		{
			// InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->WeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviousIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}

void AVandalCharacter::TryAbilityEnabled()
{
	if (AbilityEffect) // TODO Cool down
	{
		UStateEffect* NewEffect = NewObject<UStateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}

EPhysicalSurface AVandalCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = SurfaceType_Default;
	if (CharHealthComponent)
	{
		if (CharHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				const UMaterialInterface* MyMaterial = GetMesh()->GetMaterial(0);
				if (MyMaterial)
				{
					Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return Result;
}

TArray<UStateEffect*> AVandalCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void AVandalCharacter::RemoveEffect_Implementation(UStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);

	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void AVandalCharacter::AddEffect_Implementation(UStateEffect* NewEffect)
{
	Effects.Add(NewEffect);

	if (!NewEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(NewEffect, true);
		EffectAdd = NewEffect;
	}
	else
	{
		if (NewEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
		}
	}
}

void AVandalCharacter::CharDead_BP_Implementation()
{
	// BP
}

void AVandalCharacter::SetActorRotationByYaw_OnServer_Implementation(const float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

void AVandalCharacter::SetActorRotationByYaw_Multicast_Implementation(const float Yaw)
{
	if (Controller && !Controller->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void AVandalCharacter::SetMovementState_OnServer_Implementation(const EMovementState NewState)
{
	SetMovementState_Multicast(NewState);
}

void AVandalCharacter::SetMovementState_Multicast_Implementation(const EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();
}

void AVandalCharacter::TryReloadWeapon_OnServer_Implementation()
{
	if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
	{
		CurrentWeapon->InitReload();
	}
}

void AVandalCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* Anim)
{
	if (GetMesh() && GetMesh()->GetAnimInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(Anim);
	}
}

void AVandalCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void AVandalCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void AVandalCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ParticleSystem)
{
	ExecuteEffectAdded_Multicast(ParticleSystem);
}

void AVandalCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ParticleSystem)
{
	UTypes::ExecuteEffectAdded(ParticleSystem, this, FVector(0), FName("Spine_01"));
}

void AVandalCharacter::SwitchEffect(const UStateEffect* Effect, const bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			const FName NameBoneToAttached = Effect->NameBone;
			const FVector Loc = FVector(0);

			USkeletalMeshComponent* MyMesh = GetMesh();
			if (MyMesh)
			{
				UParticleSystemComponent* NewParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, MyMesh, NameBoneToAttached, Loc,
				                                                                                     FRotator::ZeroRotator, EAttachLocation::SnapToTarget,
				                                                                                     false);
				ParticleSystemEffects.Add(NewParticleSystem);
			}
		}
	}
	else
	{
		if (Effect && Effect->ParticleEffect)
		{
			if (ParticleSystemEffects.Num() > 0)
			{
				bool bIsFind = false;
				int32 i = 0;
				while (i < ParticleSystemEffects.Num() && !bIsFind)
				{
					if (ParticleSystemEffects[i] && ParticleSystemEffects[i]->Template && Effect->ParticleEffect &&
					    Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
					{
						bIsFind = true;
						ParticleSystemEffects[i]->DeactivateSystem();
						ParticleSystemEffects[i]->DestroyComponent();
						ParticleSystemEffects.RemoveAt(i);
					}
					i++;
				}
			}
		}
	}
}

void AVandalCharacter::CharDead()
{
	CharDead_BP();
	if (HasAuthority())
	{
		float TimeAnim = 0.0f;
		const int32 Rnd = FMath::RandHelper(DeathAnimations.Num());
		if (DeathAnimations.IsValidIndex(Rnd) && DeathAnimations[Rnd] && GetMesh() && GetMesh()->GetAnimInstance())
		{
			TimeAnim = DeathAnimations[Rnd]->GetPlayLength();
			// GetMesh()->GetAnimInstance()->Montage_Play(DeadAnims[rnd]);
			PlayAnim_Multicast(DeathAnimations[Rnd]);
		}

		if (GetController())
		{
			GetController()->UnPossess();
		}

		const float DecreaseAnimTimer = FMath::FRandRange(0.2f, 1.0f);

		GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &AVandalCharacter::EnableRagdoll_Multicast, TimeAnim - DecreaseAnimTimer, false);
		SetLifeSpan(20.0f);
		if (GetCurrentWeapon())
		{
			GetCurrentWeapon()->SetLifeSpan(20.0f);
		}
	}
	else
	{
		if (GetCursorToWorld())
		{
			GetCursorToWorld()->SetVisibility(false);
		}

		AttackCharEvent(false);
	}

	if (GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	}
}

void AVandalCharacter::EnableRagdoll_Multicast_Implementation()
{
	if (GetMesh())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECR_Ignore);
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float AVandalCharacter::TakeDamage(const float DamageAmount, const FDamageEvent& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	const float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (CharHealthComponent && CharHealthComponent->GetIsAlive())
	{
		CharHealthComponent->ChangeHealthValue_OnServer(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		const AProjectile* MyProjectile = Cast<AProjectile>(DamageCauser);
		if (MyProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, NAME_None, MyProjectile->ProjectileSetting.Effect,
			                               GetSurfaceType()); // to do Name_None - bone for radial damage
		}
	}

	return ActualDamage;
}

bool AVandalCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

void AVandalCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AVandalCharacter, MovementState);
	DOREPLIFETIME(AVandalCharacter, CurrentWeapon);
	DOREPLIFETIME(AVandalCharacter, CurrentIndexWeapon);
	DOREPLIFETIME(AVandalCharacter, Effects);
	DOREPLIFETIME(AVandalCharacter, EffectAdd);
	DOREPLIFETIME(AVandalCharacter, EffectRemove);
}
