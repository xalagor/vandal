// Copyright Epic Games, Inc. All Rights Reserved.

using System.Collections.Generic;
using UnrealBuildTool;

public class VandalEditorTarget : TargetRules
{
    public VandalEditorTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Editor;
        ExtraModuleNames.Add("Vandal");

        DefaultBuildSettings = BuildSettingsVersion.V2;
    }
}
